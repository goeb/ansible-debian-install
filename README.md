# ansible-debian-install

Includes roles to:

* encrypt devices (using LUKS), with support for remote unlocking during boot
* create Btrfs filesystems, including subvolumes
* debootstrapping a Debian (Buster) installation
* some basic configuration to allow public key SSH access to the new system

Running the playbook (after proper per-host configuration) should result in an
encrypted (except `/boot`), bootable Debian system, with `root` being able to
login over SSH (public key only). No further configuration will be done. This
has been tested only on KVM guests, though, a bare-metal installation may
require additional steps (configuration, additional packages, etc.).

The roles used (or rather their tasks) are executed in a non-standard way,
split into different stages. See the `include_roles` role for more details.

Detailed information is available as documentation for the individual roles.

The `stuff/filters` directory contains a custom filter and it has to be added
to Ansible's filter search path (the included `ansible.cfg` takes care of
that).

## Example Playbook

The example configuration (`ansible.cfg`) and playbook (`debian-install.yml`)
use the inventory in the `hosts` directory.

The example inventory contains one localhost entry, which will install the
Debian system to the (local) loop device `/dev/loop0` with two partitions.
Partitions have to be created manually! To create the loop device, use:

```
truncate -s 1G image
losetup -P /dev/loop0 image
```

Then use your favourite tool to create a small `/boot` partition on this loop
device (as `/dev/loop0p1`, 100 MB should be enough) and another partition for
the rest of the installation (`/dev/loop0p2`, `minbase` requires about 700 MB,
this partition will be encrypted).

The mapper device of the encrypted partition will be called `cryptroot0`, this
has to be changed if a device with this name already exists!

The files in `files/localhost-1` have to be replaced by actual public keys if
required.

The example playbook does not contain any tasks to ensure the availability of
required tools, e.g. `debootstrap`! These have to be installed manually.

## License

Copyright © 2019 Stefan Göbel < ansdebins ʇɐ subtype ˙ de >.

ansible-debian-install is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

ansible-debian-install is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
ansible-debian-install. If not, see <http://www.gnu.org/licenses/>.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
