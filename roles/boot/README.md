# ansible-debian-install / boot

Adds bootloader and kernel to the list of packages to be installed, configures
and installs Grub.

## Requirements

Requires the `mount` and `debootstrap` roles. The `mount_root` variable has to
be set to the installation's root directory.

## Stages

`prepare`
:  Adds required packages to the `debootstrap` role's package list.

`configure`
:  Sets kernel parameters in Grub's configuration.

`finish`
:  Installs Grub on disk, generates initramfs and Grub configuration.

## Role Variables

`boot_kernel`
:  The kernel package (or meta-package) to install. Must exist in the Debian
   repos. Default: `"linux-image-amd64"`.

`boot_grub_disk`
:  The device to install Grub to (with `grub-install <device>`). If this is
   left empty, `grub-install` will not be executed. Default: `""`.

`kernel_cmdln`
:  Kernel command line parameters to be set in `/etc/default/grub` for the
   `GRUB_CMDLINE_LINUX` option. Other roles may add parameters to this list as
   required, see below. Default: `[]`.

## Adding Kernel Parameters

Kernel parameters may be added using the tasks in `add_params.yml`:

```
-  include_role:
      name:           boot
      tasks_from:     add_params
   vars:
      kernel_param:   <parameter(s)>
```

The `kernel_param` variable may be a single string or a list of strings.
Parameters will be appended to the existing list, and will be added in this
order to the kernel command line as described above.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
