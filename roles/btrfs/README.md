# ansible-debian-install / btrfs

Creates Btrfs filesystems (including subvolumes).

## Requirements

Requires the `mount` and `debootstrap` roles.

## Stages

`prepare`
:  Adds required packages to the `debootstrap` role's package list.

`filesystem`
:  Creates the configured Btrfs filesystems and subvolumes.

## Role Variables

`btrfs_defaults`
:  `mkfs_opts`
   :  List of default options for the `mkfs.btrfs` command. Defaults to an
      empty list. Note that this list will NOT be merged with individual
      filesystem options (`btrfs_filesystems[*].mkfs_opts`, see below).

   `force`
   :  Whether to create filesystems on non-empty devices. The `file` command
      will be used to check the devices (any result other than `"data"` will
      be considered non-empty). Note that `mkfs.btrfs` does its own check, and
      if `force` is set to `true`, the `-f` option should be included in the
      `mkfs_opts`, too.

`btrfs_filesystems`
:  A list of filesystems to create. List items are dictionaries with the
   following keys:

   `devices`
   :  List of devices of the filesystem. Required.

   `mkfs_opts`
   :  Options for the `mkfs.btrfs` command. Defaults to
      `btrfs_defaults.mkfs_opts` if undefined, see above.

   `force`
   :  Whether to create filesystems on non-empty devices. Defaults to
      `btrfs_defaults.force`. See above for more information.

   `subvolumes`
   :  List of subvolumes to create on this filesystem, and their mount
      information. List elements are dictionaries containing the keys `path`
      (path to the subvolume relative to the filesystem's root), `mount`
      (mount point of this subvolume) and `mount_opts` (comma-separated list
      of mount options, as in `fstab`, must not be empty). Note that these
      options will be used to mount the subvolume before running
      `debootstrap`.

      To mount the top-level subvolume, set `path` to `"/"`.

      All subvolumes will be added to `/etc/fstab` in the new installation
      automatically.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
