# ansible-debian-install / debootstrap

Runs debootstrap to install Debian.

## Requirements

Requires the `mount` role. The `mount_root` variable has to be set to the
installation's root directory.

## Stages

`install`
:  Runs `debootstrap`.

## Role Variables

`debootstrap_pkgs`
:  List of packages to install in addition to `debootstrap_variant` (see
   below). Defaults to `["apt-utils", "dialog"]`.

`debootstrap_excl`
:  List of packages to exclude from installation. Defaults to
   `["apt-transport-https"]` (which is not required anymore).

`debootstrap_arch`
:  Architecture. Defaults to `"amd64"`.

`debootstrap_dist`
:  Debian version. Defaults to `"buster"`.

`debootstrap_variant`
:  Variant to install. Defaults to `"minbase"`.

`debootstrap_mirror`
:  Mirror to use. Defaults to `"http://mirrors.edge.kernel.org/debian/"`.

## Adding And Excluding Packages

Packages to include or exclude may be added using `add_pkgs.yml`:

```
-  include_role:
      name:        debootstrap
      tasks_from:  add_pkgs
   vars:
      include:     <package(s)>
      exclude:     <package(s)>
```

The `include` and `exclude` variables may be single strings or lists of
strings of package names. Both parameters are optional.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
