# ansible-debian-install / encrypt

Encrypts devices and configures the system for remote unlocking during boot
(running a DropBear server in the initramfs).

## Requirements

Requires the `boot`, `debootstrap` and `mount` roles. The `mount_root`
variable has to be set to the installation's root directory. The variables
`network_ip4`, `network_gw` and `network_if` have to be set, too (these are
the same variables as used by the `network` role).

This role requires the `netaddr` Python module on the Ansible controller.

## Stages

`prepare`
:  Adds required packages to `debootstrap`'s package list and adds required
   kernel parameters.

`filesystem`
:  Encrypts (and unlocks) the configured devices.

`configure`
:  Configures `crypttab` and DropBear.

`always`
:  Closes the unlocked devices.

## Role Variables

`encrypt_defaults`
:  Dictionary containing the default encryption parameters. These may be
   overridden for individual devices if required. Supported options are:

   `cipher`
   :  Cipher to use (with `cryptsetup --cipher`). Defaults to
      `"aes-xts-plain64"`.

   `hash`
   :  Hash to use (with `cryptsetup --hash`). Defaults to `"sha256"`.

   `key_size`
   :  Key size to use (with `cryptsetup --key-size`). Defaults to `"256"`.

   `iter_time`
   :  Iteration time to use (with `cryptsetup --iter-time`). Defaults to
      `"5000"`.

   `luksformat_args`
   :  List of additional arguments for `cryptsetup luksFormat`. Defaults to
      `[ "--type", "luks2" ]`.

   `options`
   :  Options to add to `crypttab` for the device. Defaults to `"luks"`.

   `force`
   :  Whether to encrypt the device even if it already contains a LUKS
      signature. Defaults to `false`.

`encrypt_devices`
:  List of devices to encrypt, empty by default. Each list item must be a
   dictionary, with the following keys:

   `device`
   :  The (unencrypted) device (full path).

   `mapper`
   :  The name of the mapper device when the device has been opened (without
      `"/dev/mapper/"` prefix).

   `passphrase`
   :  The passphrase, no trailing newline. Use the vault for this!

   Additionally, all options that may be set in `encrypt_defaults` may be
   included, to override the default values for individual devices.

`encrypt_dropbear_port`
:  The default port for DropBear to listen on during boot. Defaults to
   `22222`.

## Files

The file `files/{{ inventory_hostname }}/dropbear.pub` will be copied to the
installation as the public key for the DropBear daemon. This file must exist
and must be a valid SSH public key!

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
