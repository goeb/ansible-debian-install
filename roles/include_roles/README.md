# include_roles

A role to include other roles, in multiple stages.

Example configuration:

```
include_roles:
   - role1
   - role2
   - role3

include_stages:
   - stage1
   - stage2

include_always:
   - always
```

With this configuration, tasks will be called in the following order:

- `stage1`
   - `role1`/`stage1`
   - `role2`/`stage1`
   - `role3`/`stage1`
- `stage2`
   - `role1`/`stage2`
   - `role2`/`stage2`
   - `role3`/`stage2`
- `always`
   - `role3`/`always`
   - `role2`/`always`
   - `role1`/`always`

I.e., the outer loop iterates over the defined stages, and for every stage the
inner loop iterates over the included roles. To implement a stage, a role must
put its tasks for that stage in the `<role>/tasks/<stage name>.yml` file, it
will then be included like this:

```
- include_role:
   name: <role name>
   tasks_from: <stage>.yml
```

Stages may be omitted if not required. All roles' top level directories must
be located in the same directory as the `include_roles` role itself.

Note that, by default, roles will be called in reverse order for all stages
defined in the `include_always` list (the stages themselves will be run in the
defined order). This behaviour may be changed by setting the variable
`include_always_reverse` to `false`. Roles in this list will always be called
at the end, even if a task fails during the regular stages (called in the
`always` part of an Ansible `block` - a failure in one of these tasks will
still stop playbook execution).

## Role Variables

`include_roles`
:  List of roles to include. Order matters. Defaults to an empty list.


`include_stages`
:  List of stages to run, in the defined order. Defaults to an empty list.

`include_always`
:  Stages to always run after the `include_stages` - even on failure. Defaults
   to an empty list.

`include_always_reverse`
:  Whether to call roles in reversed order for stages in `include_always`.
   Defaults to `true`.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
