# ansible-debian-install / mount

Mounts devices - regular devices before running `debootstrap` and special
devices afterwards to allow running `chroot` commands. Also configures the
installation's `fstab`.

## Stages

`filesystem`
:  Creates a temporary root directory (if required) and mounts all regular
   filesystems (as configured).

`postinstall`
:  Mounts special filesystems (e.g. `/dev`).

`configure`
:  Configures the installation's `fstab`.

`always`
:  Unmounts all mounted devices and removes the temporary mount point if
   required.

## Role Variables

`mount_root`
:  The installation's root directory. If this is empty (the default), a
   temporary directory will be created (and removed at the end). If set, it
   will be used without any further checks (and not removed at the end).

`mount_special`
:  List of special mounts required to run commands in the installation
   `chroot`. They will be mounted in the order specified. The target
   directories will be created if required. The list items are lists
   containing the filesystem type (e.g. `"proc"`) and the mount point (e.g.
   `"/proc"`), in that order. See `defaults/main.yml` for the defaults, which
   should be sufficient in most cases.

`mount`
:  List of regular filesystem mounts. Items in this list must be dictionaries,
   with the following keys (all keys must be present and not empty):

   `device`
   :  The device to mount, e.g. `"/dev/sda1"`.

   `mount`
   :  The mount point, absolute path (as in `fstab`), e.g. `"/var"`.

   `options`
   :  Mount options, as comma-separated string (as in `fstab`), e.g.
      `"noatime,nodev,noexec,nosuid"`.

      Note that the options will be used when mounting the devices before
      running `debootstrap`. This means that, for example, `/var` must not be
      set `noexec` in this configuration!

   Entries in the `mount` list may be added in the configuration, but keep in
   mind that other roles may also add their devices (e.g. the `btrfs` role
   will automatically add entries for all configured subvolumes).

   The `mount` list is empty by default.

## Adding Devices To Mount

Instead of adding devices to the `mount` list manually, the tasks in
`add_mount.yml` may be used:

```
-  include_role:
      name:       mount
      tasks_from: add_mount
   vars:
      device:     <device path>
      type:       <filesystem type>
      target:     <mount point>
      options:    <any,mount,options>
```

Note that all variables are required and must not be empty!

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
