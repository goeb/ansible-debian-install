# ansible-debian-install / network

Basic `systemd-networkd` configuration. Static IPv4 for one interface only!

## Requirements

Requires `systemd` to be included in the installation (which should include
`systemd-networkd`).

## Stages

`configure`
:  Configures and enables `systemd-networkd`.

## Role Variables

All variables are empty by default.

`network_if`
:  Interface name, e.g. `"eth0"`. Leave empty to not configure
   `systemd-networkd`.

`network_ip4`
:  IPv4 address and netmask for the interface (CIDR notation), e.g.
   `"192.168.1.42/24"`.

`network_dns`
:  IPv4 of the DNS server, e.g. `"192.168.1.1"`.

`network_gw`
:  IPv4 of the gateway, e.g. `"192.168.1.1"`.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
