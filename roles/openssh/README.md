# ansible-debian-install / openssh

Installs and configures the OpenSSH server.

**Note**: Only the port will be changed, all other options will keep Debian's
default values, which includes the permission to login via public key for the
root user (`PermitRootLogin` is not set, but defaults to `prohibit-password`,
password logins for root are disabled).

## Requirements

Requires the `mount` and `debootstrap` roles. The `mount_root` variable has to
be set to the installation's root directory.

## Stages

`prepare`
:  Adds required packages to the `debootstrap` role's package list.

`configure`
:  Configures the OpenSSH server.

## Role Variables

`openssh_port`
:  The OpenSSH server will listen on this port. Defaults to `12345`.

## Files

The file `files/{{ inventory_hostname }}/root.pub` will be copied to the
installation as the public key for the root user (`~/.ssh/authorized_keys`).
This file must exist and must be a valid SSH public key!

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
