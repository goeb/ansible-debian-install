# ansible-debian-install / pause

Pauses playbook execution to allow for manual inspection of the installation
before everything is cleaned up (i.e. filesystems unmounted, mapper devices
closed etc.).

## Stages

`finish`
:  Pauses playbook execution - waits for user confirmation before continuing.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
