# ansible-debian-install / systemd

Adds packages required for systemd to the `debootstrap` role's package list
and sets the `init` kernel parameter (using the `boot` role).


## Requirements

Requires the `boot` and `debootstrap` roles.

## Stages

`prepare`
:  Adds packages and kernel parameters.

[modeline]: # :indentSize=3:tabSize=3:noTabs=true:mode=markdown:maxLineLen=78:
