#!/usr/bin/python

class FilterModule (object):

   def filters (self):
      return { "not_root" : self.not_root }

   def not_root (self, path):

      """Fail if path is empty or refers to the root directory."""

      import os.path
      import ansible.errors

      if path == "" or os.path.realpath (path) == "/":
         raise ansible.errors.AnsibleFilterError ("Invalid path: %s" % path)

      return path

# :indentSize=3:tabSize=3:noTabs=true:mode=python:maxLineLen=115:
